#include <iostream>
#include <cstdlib>
#include "circular_arr_queue.h"
using namespace std;

int main(int argc, char **argv){
  Queue queue;
  if (argc >= 2){
    for (int i = 1; i < argc; i++){
      cout << "equeued: " << (queue.enqueue(atoi(argv[i])) ? "true" : "false") << endl;
    }
  }
  cout << "equeued: " << (queue.enqueue(20) ? "true" : "false") << endl;
  cout << "equeued: " << (queue.enqueue(5) ? "true" : "false") << endl;
  cout << "equeued: " << (queue.enqueue(2) ? "true" : "false") << endl;
  cout << "equeued: " << (queue.enqueue(7) ? "true" : "false") << endl;
  cout << "equeued: " << (queue.enqueue(9) ? "true" : "false") << endl;
  cout << "equeued: " << (queue.enqueue(3) ? "true" : "false") << endl;
  cout << "front of queue: " << queue.front() << endl;
  cout << "size of queue: " << queue.size() << endl;
  cout << "is empty: " << (queue.empty() ? "true" : "false") << endl;
  cout << "dequeued: " << (queue.dequeue() ? "true" : "false") << endl;
  cout << "dequeued: " << (queue.dequeue() ? "true" : "false") << endl;
  cout << "dequeued: " << (queue.dequeue() ? "true" : "false") << endl;
  cout << "dequeued: " << (queue.dequeue() ? "true" : "false") << endl;
  cout << "dequeued: " << (queue.dequeue() ? "true" : "false") << endl;
  cout << "is empty: " << (queue.empty() ? "true" : "false") << endl;
  cout << "dequeued: " << (queue.dequeue() ? "true" : "false") << endl;
  cout << "dequeued: " << (queue.dequeue() ? "true" : "false") << endl;
  cout << "is empty: " << (queue.empty() ? "true" : "false") << endl;
  cout << "front of queue: " << queue.front() << endl;

  return 0;
}
