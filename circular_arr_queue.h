#ifndef __CIRCULAR_ARR_QUEUE__
#define __CIRCULAR_ARR_QUEUE__

#include <iostream>

class Queue {
  private:
    unsigned int Q[20]; // queue arr size of 20
    unsigned int rearIndex = 0; // index of Q rear
    unsigned int frontIndex = 0; // index of Q front
    unsigned int currentNumElements = 0; // current num elements in queue
  public:
    int size(void) const; //returns size of queue if empty returns -1
    int front(void) const; // returns without poping the first element
    bool empty(void) const; // returns true of queue is empty
    bool enqueue (int e); // returns true if succesfull in adding e to queue
    bool dequeue(void); // returns true if succesfull in poping front element 
};

#endif

