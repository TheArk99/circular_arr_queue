#include "circular_arr_queue.h"

int Queue::size(void) const{
  return empty() ? currentNumElements : currentNumElements + 1;
}

bool Queue::empty(void) const{
   return currentNumElements == 0;
}

int Queue::front(void) const{
  if (empty()){
    return -1;
  }
  return Q[frontIndex];
}

bool Queue::enqueue (int e){
  bool complete = false;

  if ((!(e < 0)) && currentNumElements < 19){
    Q[rearIndex] = e;
    rearIndex = (rearIndex + 1) % 20;  // need to do mod of size
    currentNumElements = (currentNumElements + 1) % 20;
    complete = true;
  }
  
  if (empty()){  // EH should be if (full())
    complete = false;
  }

  return complete;
}

bool Queue::dequeue(void){
  bool complete = false;
  
  if (!empty()){
    currentNumElements = (currentNumElements - 1) % 20;
    frontIndex = (frontIndex + 1) % 20;  // need to do mod of size
    complete = true;
  }

  return complete;
}
